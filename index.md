---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
# Testés et approuvés 😉

En cours de construction...

## Bloquer la pub

 <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/uBlock%20Origin.png">
    </div>
    <div>
      <h2>uBlock Origin</h2>
      <p>Une extension vous laissant le choix de bloquer, ou non, les publicités des sites web que vous rencontrez.</p>
      <div>
        <a href="https://framalibre.org/notices/ublock-origin.html">Vers la notice Framalibre</a>
        <a href="https://github.com/gorhill/uBlock">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/SponsorBlock.png">
    </div>
    <div>
      <h2>SponsorBlock</h2>
      <p>SponsorBlock est une extension et une API pour sauter des segments de sponsor dans des vidéos YouTube.</p>
      <div>
        <a href="https://framalibre.org/notices/sponsorblock.html">Vers la notice Framalibre</a>
        <a href="https://sponsor.ajay.app/">Vers le site</a>
      </div>
    </div>
  </article>

## Lire les courriels

### Sur ordinateur

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Thunderbird.png">
    </div>
    <div>
      <h2>Thunderbird</h2>
      <p>Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/thunderbird.html">Vers la notice Framalibre</a>
        <a href="https://www.thunderbird.net/fr/">Vers le site</a>
      </div>
    </div>
  </article>

### Sur ordiphone

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FairEmail.png">
    </div>
    <div>
      <h2>FairEmail</h2>
      <p>Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité.</p>
      <div>
        <a href="https://framalibre.org/notices/fairemail.html">Vers la notice Framalibre</a>
        <a href="https://email.faircode.eu/">Vers le site</a>
      </div>
    </div>
  </article>

## Dessiner

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/GIMP.png">
    </div>
    <div>
      <h2>GIMP</h2>
      <p>Un puissant logiciel d'édition et de retouche d'images.</p>
      <div>
        <a href="https://framalibre.org/notices/gimp.html">Vers la notice Framalibre</a>
        <a href="https://www.gimp.org/">Vers le site</a>
      </div>
    </div>
  </article>

### Modifier des pdf

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Inkscape.png">
    </div>
    <div>
      <h2>Inkscape</h2>
      <p>Un puissant logiciel de dessin vectoriel.</p>
      <div>
        <a href="https://framalibre.org/notices/inkscape.html">Vers la notice Framalibre</a>
        <a href="https://inkscape.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/LibreOffice%20Draw.png">
    </div>
    <div>
      <h2>LibreOffice Draw</h2>
      <p>Pour vos dessins : du simple texte illustré aux graphiques vectoriels fouillés.</p>
      <div>
        <a href="https://framalibre.org/notices/libreoffice-draw.html">Vers la notice Framalibre</a>
        <a href="http://fr.libreoffice.org/discover/draw/">Vers le site</a>
      </div>
    </div>
  </article>

## Regarder des vidéos

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Piped.png">
    </div>
    <div>
      <h2>Piped</h2>
      <p>Invidious, mais avec des fonctionnalités supplémentaires.</p>
      <div>
        <a href="https://framalibre.org/notices/piped.html">Vers la notice Framalibre</a>
        <a href="https://piped.kavin.rocks/">Vers le site</a>
      </div>
    </div>
  </article>

## Réseauter

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Tusky.png">
    </div>
    <div>
      <h2>Tusky</h2>
      <p>Un client Mastodon pour Android léger.</p>
      <div>
        <a href="https://framalibre.org/notices/tusky.html">Vers la notice Framalibre</a>
        <a href="https://github.com/tuskyapp/Tusky">Vers le site</a>
      </div>
    </div>
  </article>

## Collaboration (privée)

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/CryptPad.png">
    </div>
    <div>
      <h2>CryptPad</h2>
      <p>CryptPad est une suite bureautique collaborative chifrée de bout en bout et open-source.</p>
      <div>
        <a href="https://framalibre.org/notices/cryptpad.html">Vers la notice Framalibre</a>
        <a href="https://cryptpad.org">Vers le site</a>
      </div>
    </div>
  </article>

## Créer des sondages

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Framadate.png">
    </div>
    <div>
      <h2>Framadate</h2>
      <p>Organiser des rendez-vous simplement, librement.</p>
      <div>
        <a href="https://framalibre.org/notices/framadate.html">Vers la notice Framalibre</a>
        <a href="https://framadate.org/">Vers le site</a>
      </div>
    </div>
  </article> 